#include <em_cmu.h>
#include <em_emu.h>
#include <stdbool.h>
#include <em_leuart.h>
#include <em_gpio.h>
#include <intrinsics.h>
#include "radio.h"
#include "emul.h"

/*******************************************************************************
 *******************************   DEFINES   ***********************************
 ******************************************************************************/
#define NUM_SENSORS 5
#define BYTEHIGH(v)   (*(((unsigned char *) (&v) + 1)))
#define BYTELOW(v)  (*((unsigned char *) (&v)))

typedef struct{
  uint32_t address;
  uint32_t counter;
  uint8_t  type;
  uint8_t  cowCond;
  uint8_t  cowActivity[4];
  uint8_t  temp1;
  uint8_t  temp2Low;
  uint8_t  temp2High;
  uint8_t  temp3;
  uint8_t  temp4;
  uint8_t  pulse;
  uint8_t  aes128[12];
} message_t;



/*******************************************************************************
 ***************************   static variables*********************************
 ******************************************************************************/
static sensor_t sensor[NUM_SENSORS];
const static uint32_t sensorNumber[NUM_SENSORS] = {1,2,3,4,5}; //sensors serial numbers
const static band_t sensorFreq[NUM_SENSORS] = {band0,band1,band2,band3,band4};
static uint32_t sensorTime[NUM_SENSORS];
static sensor_t* currentSensor = &sensor[0];
static message_t message;
static  uint32_t hour;
//static uint8_t messageLen = sizeof(message);
/*******************************************************************************
 ********************   LOCAL FUNCTIONS PROTOTYPES  ****************************
 ******************************************************************************/
void formDataMessage(void);
/*******************************************************************************
 **************************   GLOBAL FUNCTIONS   *******************************
 ******************************************************************************/

/***************************************************************************//**
 * @brief create message for sending
 *   
 * @param[in] counter - seconds counter
 *  
 * @param[in] data - reference to massive for message
 *
 * @param[in] data - reference to massive for message
 *
 * @return   true if time to send message
 *   
 ******************************************************************************/
bool emulMessage(uint32_t counter,uint8_t* data, uint8_t dataLen)
{
  uint32_t secondInHour = counter % hour;   // which second inside the hour
  bool isMessage = false;
  for(int i = 0; i < NUM_SENSORS; i++)
  {
    if(secondInHour == sensor[i].time)
    {
      currentSensor = &sensor[i];
      isMessage = true;
      break;
    }
  }
  if(isMessage)
  {
    currentSensor->counter++;
    message.address = currentSensor->number;
    message.counter = currentSensor->counter;
    message.type = 0;
    formDataMessage();
    for(int i = 0; i < dataLen;i++)data[i] = (*(((uint8_t *) (&message) + i)));  //send bytes from struct to array
  }
  return isMessage;
}
/***************************************************************************//**
 * @brief  check which sensor has to send a message 
 *
 *@return   band of current sensor 
 *     
 ******************************************************************************/
band_t emulBand(void)
{
  return currentSensor->frequency;
}

/***************************************************************************//**
 * @brief initialize parameters of emulated sensors
 *   
 * @param[in] isDebug - if debug == 1 than hour = 360 sec for quicker debugging
 *                               == 2 hour = 36 sec quickest mode
 *                               default hour = 3600 sec normal mode
 *  
 *   
 ******************************************************************************/
void emulInit(uint8_t debug)
{
  if(debug == 1) hour = 360;
  else if(debug == 2) hour = 36;
  else hour = 3600;
  for(int i = 0; i < NUM_SENSORS; i++)sensorTime[i] = (hour*i)/5;
 
  for(int i = 0; i < NUM_SENSORS; i++)
  {
    sensor[i].counter = 0;
    sensor[i].frequency = sensorFreq[i];
    sensor[i].number = sensorNumber[i];
    sensor[i].time = sensorTime[i];
  }
  
}

/*******************************************************************************
 **************************   LOCAL FUNCTIONS   *******************************
 ******************************************************************************/
/***************************************************************************//**
 * @brief form data fields of message
 *   
 * @param[in] message - reference on message to form
 *  
 *   
 ******************************************************************************/
 const uint8_t cowCond[][NUM_SENSORS] = {{0x5A, 0x00, 0x55, 0xAA, 0xAA},            //1 hour
                                         {0x01, 0x55, 0x80, 0x06, 0xAA},
                                         {0x55, 0x56, 0x01, 0xAA, 0x00},
                                         {0x80, 0x65, 0x55, 0xAA, 0x00},
                                         {0x55, 0xAA, 0xAA, 0x95, 0x00},
                                         {0x96, 0xA9, 0xA9, 0x55, 0x55},
                                         {0xAA, 0x5A, 0xA2, 0x80, 0x55},
                                         {0xF3, 0x95, 0x2A, 0x16, 0x54}};

const uint8_t cycleCond = sizeof(cowCond)/NUM_SENSORS;

const uint8_t cowActiv[][4] = {{0x17, 0x23, 0x75, 0x14},
                                {0x08, 0x03, 0x32, 0x45},
                                {0x12, 0x13, 0x03, 0x10},
                                {0x67, 0x05, 0x08, 0x07},
                                {0x11, 0x09, 0x04, 0x03},
                                {0x20, 0x19, 0x16, 0x44},
                                {0x31, 0x07, 0x05, 0x04},
                                {0x06, 0x08, 0x11, 0x16}};

const uint8_t cycleActiv = sizeof(cowActiv)/4; 

void formDataMessage(void)
{
  uint16_t temp2 = 400 + (currentSensor->number - 1)*10;
  message.cowCond = cowCond[(currentSensor->counter)%cycleCond][currentSensor->number - 1];
  message.cowActivity[0] = cowActiv[(currentSensor->counter)%cycleActiv][0] + (currentSensor->number - 1);
  message.cowActivity[1] = cowActiv[(currentSensor->counter)%cycleActiv][1] + (currentSensor->number - 1);
  message.cowActivity[2] = cowActiv[(currentSensor->counter)%cycleActiv][2] + (currentSensor->number - 1);
  message.cowActivity[3] = cowActiv[(currentSensor->counter)%cycleActiv][3] + (currentSensor->number - 1);
  message.pulse = 40 + (currentSensor->number - 1);
  message.temp1 = 220 + (currentSensor->number - 1);
  message.temp2High = BYTEHIGH(temp2);
  message.temp2Low = BYTELOW(temp2);
  message.temp3 = 210 + (currentSensor->number - 1);
  message.temp4 = 200 + (currentSensor->number - 1);
}