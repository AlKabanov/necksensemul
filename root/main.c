/**************************************************************************//**
 * @file
 * @brief template for agro modem
 * @version 1.0.0
 *
 ******************************************************************************/

#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include <intrinsics.h>
#include "em_device.h"
#include "em_chip.h"
#include "em_emu.h"
#include "em_cmu.h"
#include "em_gpio.h"
#include "em_rtc.h"

#include "gpio.h"
#include "rtc.h"
#include "spi.h"
#include "radio.h"
#include "uart.h"
#include "buffer.h"
#include "emul.h"

#define POWER 12
#define ONE_SEC  (32768)
#define TWO_SEC  ((ONE_SEC*2)-1)
#define MSEC_500 ((ONE_SEC/2)-1)
#define MSEC_2 ((ONE_SEC/500)-1)

#define BYTEHIGH(v)   (*(((unsigned char *) (&v) + 1)))
#define BYTELOW(v)  (*((unsigned char *) (&v)))
   
#define STANDBY_BAND band7

#define ZERO_MESSAGE_EN 

int16_t temperature = 45, temperature1 = 0, temperature2 = 0;	
int fromUART;
uint8_t data[32];  //message to send to BS
uint8_t dataLen = sizeof(data);	
receive_t RADIO_IN = {20,0,0};	//default period = 20mS, dataPtr = 0,lastSent=0,dataValid=0
uint8_t logCounter = 0;
uint8_t sendCounter = 1;
uint32_t startTimer, stopTimer;
uint32_t counter = 0;


//function prototypes
void sleepMode(void); 
/**************************************************************************//**
 * @brief Update clock and wait in EM2 for RTC tick.
 *****************************************************************************/
  
void clockLoop(void)
{
  emulInit(2);
  while (1)
  {
       
    if(getRXDoneFlag())
    {
      radioReadRXBuf(&RADIO_IN); //check message
    }
//    if(gpioIsButtonPressed()==true)  //send go to the ready mode message
    fromUART = uartGetChar();
    if(fromUART > 0)
    {
      //gpioClearButtonPressed();
      gpioClearLED();
      switch(fromUART)
      {
      case 'n': 
      counter = 0;
      emulInit(0);
      uartSendArr("normal mode, hour = 3600s \n\r", sizeof("normal mode, hour = 3600s \n\r"));
      break;
      
      case 'q':  
      counter = 0;
      emulInit(2);
      uartSendArr("quick mode, hour = 36s \n\r", sizeof("quick mode, hour = 36s \n\r"));
      break;
     
      case 'd': 
      counter = 0;
      emulInit(1);
      uartSendArr("debug mode, hour = 360s \n\r", sizeof("debug mode, hour = 360s \n\r"));
      break;
      
      case 'h': 
      uartSendArr(" d - debug mode \n\r q - quick mode \n\r n - normal mode \n\r", sizeof(" d - debug mode \n\r q - quick mode \n\r n - normal mode \n\r"));
      break;
        
      } //end_switch(fromUART)

    }
    gpioToggleLED();
    
    if(emulMessage(counter,data,dataLen))
    {
#ifndef ZERO_MESSAGE_EN
      uartSendArr("sending message \n\r", sizeof("sending message \n\r"));
      radioTest(data, dataLen, POWER,false,emulBand());
#else
      for(int i = 0; i < dataLen; i++)data[i] = 0;//create zero message
      uartSendArr("sending zero message \n\r", sizeof("sending zero message \n\r"));
      radioTest(data, dataLen/4, 0, false, STANDBY_BAND);
#endif
      
      while(!getTXDoneFlag()); //wait for transmission to finish
    }
    counter++;
    rtcSetWakeUp(ONE_SEC);  //sleep for 1 sec
    sleepMode();
   
  }
}

/**************************************************************************//**
 * @brief  Main function
 *****************************************************************************/
int main(void)
{
  /* Chip errata */
  CHIP_Init();

  /* Ensure core frequency has been updated */
  SystemCoreClockUpdate();
  
  gpioSetup();
  
  spiSetup();

  /* Setup RTC to generate an interrupt every minute */
  rtcSetup();
  
  uartInit(9600,true); //538uA
  
  
  gpioSetLED();
  rtcWait(200);
  gpioClearLED();
  

  /* Main function loop */
  clockLoop();

  return 0;
}

void sleepMode(void)
{
  //gpioPULSEPWROFF();
  
  RTC_IntClear(RTC_IFC_COMP0);
  RTC_IntDisable(RTC_IEN_COMP0 );
  //NVIC_DisableIRQ(GPIO_ODD_IRQn);
  RTC_IntEnable(RTC_IEN_COMP0 );
  radioSleep();
  EMU_EnterEM2(true);
  
}