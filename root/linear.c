#include <em_cmu.h>
#include <em_emu.h>
#include <stdbool.h>
#include <em_leuart.h>
#include <em_gpio.h>
#include <intrinsics.h>
#include "linear.h"

/*******************************************************************************
 *******************************   DEFINES   ***********************************
 ******************************************************************************/


/*******************************************************************************
 **************************   GLOBAL FUNCTIONS   *******************************
 ******************************************************************************/

/***************************************************************************//**
 * @brief
 *   Add a new data to the buffer, 
 *   set storePointer
 * @param[in] input - data for enter into the table of linearization
 *  
 *
 *@return   linearized value
 *   
 *   
 *   
 ******************************************************************************/
const int16_t  tableIn  [] = {362,4  ,-360,-723,-1076,-1411,-1722};
const int16_t  tableOut [] = {100,150,200 ,250 ,300  ,350  ,400   };
uint8_t lenTab = sizeof(tableIn)/sizeof(int16_t);
int16_t linearMake( int16_t input)
  {
    int16_t output = 0;
    uint8_t index = 1;
    if(input >= tableIn[0])return tableOut[0];  
    if(input <= tableIn[lenTab-1]) return tableOut[lenTab-1]; //boundary situations
    for(; index < lenTab; index ++)
    {
      if (input >= tableIn[index])break;
    }
    output = tableOut[index] - ((input - tableIn[index])*50)/(tableIn[index-1]-tableIn[index]);  
    return output;
  }

