
#include <em_cmu.h>
#include <em_emu.h>

#include <em_gpio.h>
#include "gpio.h"

#define LED_PORT      gpioPortC           
#define LED_BIT       15				
#define LED_MASK      (1 << LED_BIT)

#define PIN_SPI_CS                13			//CSN = PE13
#define PORT_SPI_CS               gpioPortE

#define RST_PORT gpioPortB
#define RST_PIN  14	

#define TEMP_PWR_PORT gpioPortB
#define TEMP_PWR_BIT  11
#define TEMP_PWR_EN   GPIO_PinModeSet(TEMP_PWR_PORT, TEMP_PWR_BIT, gpioModePushPull, 0)
#define TEMP_PWR_ON   GPIO_PinOutSet(TEMP_PWR_PORT, TEMP_PWR_BIT)
#define TEMP_PWR_OFF  GPIO_PinOutClear(TEMP_PWR_PORT, TEMP_PWR_BIT)

#define PULS_PWR_PORT gpioPortC
#define PULS_PWR_BIT  0
#define PULS_PWR_EN   GPIO_PinModeSet(PULS_PWR_PORT, PULS_PWR_BIT, gpioModePushPull, 0);\
                      GPIO_DriveModeSet(PULS_PWR_PORT,gpioDriveModeHigh)

#define PULS_PWR_ON   GPIO_PinOutSet(PULS_PWR_PORT, PULS_PWR_BIT)
#define PULS_PWR_OFF  GPIO_PinOutClear(PULS_PWR_PORT, PULS_PWR_BIT)

#define LED_EN  GPIO_PinModeSet(LED_PORT, LED_BIT, gpioModePushPull, 0)
#define LED_DIS GPIO_PinModeSet(LED_PORT, LED_BIT, gpioModeDisabled, 0)
#define LED_ON  GPIO_PinOutSet(LED_PORT, LED_BIT)
#define LED_OFF GPIO_PinOutClear(LED_PORT, LED_BIT)
#define LED_TOGGLE GPIO_PinOutToggle(LED_PORT, LED_BIT)

#define LDO_PORT      gpioPortF           
#define LDO_BIT       2		
#define LDO_EN  GPIO_PinModeSet(LDO_PORT, LDO_BIT, gpioModePushPull, 0)
#define LDO_DIS GPIO_PinModeSet(LDO_PORT, LDO_BIT, gpioModeDisabled, 0)
#define LDO_ON  GPIO_PinOutSet(LDO_PORT, LDO_BIT)
#define LDO_OFF GPIO_PinOutClear(LDO_PORT, LDO_BIT)
                        
#define BUTTON_PORT gpioPortA
#define BUTTON_PIN 0
#define BUTTON_MASK (1<<BUTTON_PIN)                      

#define CHEW_PORT gpioPortD
#define CHEW_PIN 6
#define CHEW_MASK (1<<CHEW_PIN)
                        
#define RUM_PORT gpioPortC
#define RUM_PIN 1
#define RUM_MASK (1<<RUM_PIN)


/** Array of GPIO IRQ hooks for system/user/odd/even IRQs */
GPIO_hook_t GPIO_Hooks[hookSize];

static void buttonPressedInterrupt(void);
static void chewPressedInterrupt(void);
static void rumPressedInterrupt(void);
/**************************************************************************//**
 * @brief Setup GPIO interrupt to set the time
 *****************************************************************************/
void gpioSetup(void)
{
  /* Enable GPIO clock */
  CMU_ClockEnable(cmuClock_GPIO, true);
  
  GPIO_PinModeSet(PORT_SPI_CS, PIN_SPI_CS, gpioModePushPull, 1);
  
  //  Configure PC14 as input 
  GPIO_PinModeSet(RF_DIO0_PORT, RF_DIO0_PIN, gpioModeInput, 1);

  // Set rasing edge interrupt 
  GPIO_IntConfig(RF_DIO0_PORT, RF_DIO0_PIN, true, false, true);
 // NVIC_ClearPendingIRQ(GPIO_EVEN_IRQn);
 // NVIC_EnableIRQ(GPIO_EVEN_IRQn);
  
  //  Configure PA0 (Button) as input 
  GPIO_PinModeSet(BUTTON_PORT, BUTTON_PIN, gpioModeInput, 1);

  // Set falling edge interrupt 
  GPIO_IntConfig(BUTTON_PORT, BUTTON_PIN, false, true, true);
  //NVIC_ClearPendingIRQ(GPIO_EVEN_IRQn);
  //NVIC_EnableIRQ(GPIO_EVEN_IRQn);
  
  //  Configure PD6 (CHEW) as input 
  GPIO_PinModeSet(CHEW_PORT, CHEW_PIN, gpioModeInputPull, 1);

  // Set falling edge interrupt 
  GPIO_IntConfig(CHEW_PORT, CHEW_PIN, false, true, true);
  
  NVIC_ClearPendingIRQ(GPIO_EVEN_IRQn);
  NVIC_EnableIRQ(GPIO_EVEN_IRQn);
  
  //  Configure PC1 (RUMINATION) as input 
  GPIO_PinModeSet(RUM_PORT, RUM_PIN, gpioModeInputPull, 1);
  
  // Set falling edge interrupt 
  GPIO_IntConfig(RUM_PORT, RUM_PIN, false, true, true);
  
  NVIC_ClearPendingIRQ(GPIO_ODD_IRQn);
  NVIC_EnableIRQ(GPIO_ODD_IRQn);
  
  

 
  
  // set callback fo interrupt from Button(PA0) 
  GPIO_SetCallback(button, buttonPressedInterrupt, BUTTON_MASK);
  
  // set callback fo interrupt from START(PD6) 
  GPIO_SetCallback(chew, chewPressedInterrupt, CHEW_MASK);
  
  // set callback fo interrupt from STOP(PD4) 
 // GPIO_SetCallback(rumination, rumPressedInterrupt, RUM_MASK); No callback for odd
  
  //  Configure PC1 as output 0 
  //GPIO_PinModeSet(gpioPortC, 1,  gpioModePushPull, 0); //ground
/*
  // Set falling edge interrupt 
  GPIO_IntConfig(gpioPortB, 10, false, true, true);
  NVIC_ClearPendingIRQ(GPIO_EVEN_IRQn);
  NVIC_EnableIRQ(GPIO_EVEN_IRQn);

  // Configure PB9 as input 
  GPIO_PinModeSet(gpioPortB, 9, gpioModeInput, 0);
*/
  
  LED_EN;
  LDO_EN;
  TEMP_PWR_EN;
  PULS_PWR_EN;
}

void gpioSetLED(void)
{
  LED_ON;
}

void gpioClearLED(void)
{
  LED_OFF;
}

void gpioToggleLED(void)
{
  LED_TOGGLE;
}
void gpioLDOOn(void)
{
  LDO_ON;
}

void gpioLDOOff(void)
{
  LDO_OFF;
}

void gpioTEMPPWRON(void)
{
  TEMP_PWR_ON;
}

void gpioTEMPPWROFF(void)
{
  TEMP_PWR_OFF;
}
void gpioPULSEPWRON(void)
{
  PULS_PWR_ON;
}

void gpioPULSEPWROFF(void)
{
  PULS_PWR_OFF;
}

// set radio RST pin to given value (or keep floating!)
void gpioRST(uint8_t val)
{
  if(val == 0 || val == 1)
  { // drive pin
       GPIO_PinModeSet(RST_PORT, RST_PIN, gpioModePushPull, 0);
       if (val)GPIO_PinOutSet(RST_PORT, RST_PIN);
       else    GPIO_PinOutClear(RST_PORT, RST_PIN);
  } else 
    { // keep pin floating
        GPIO_PinModeSet(RST_PORT, RST_PIN, gpioModeDisabled, 1);
    }
}

// val ==1  => tx 1, rx 0 ; val == 0 => tx 0, rx 1
void gpioRXTX (uint8_t val)
{
	//not used 
}

// set radio NSS pin to given value
void gpioNSS (uint8_t val)
{
  if (val) GPIO_PinOutSet(PORT_SPI_CS, PIN_SPI_CS);	//  SPI Disable
		
  else GPIO_PinOutClear(PORT_SPI_CS, PIN_SPI_CS);	//  SPI Enable (Active Low)
}		
/*******************************************************************************
  @brief
 *   Read the pad value for a button pin.
 *
 * @param None
 *
 * @return
 *   The pin value, false or true.
*******************************************************************************/
bool gpioButtonPinGet(void)
{
  if(GPIO_PinInGet(BUTTON_PORT,BUTTON_PIN) == 0)return false;
  return true;
}
 
/**************************************************************************//**
 * @brief GPIO Interrupt handler (PB9)
 *        Sets the hours
 *****************************************************************************/

bool sleepFlag = false;



bool gpioGetSleepFlag(void)
{
  bool temp = sleepFlag;
  sleepFlag = false;
  return temp;
}

/************************************************************************
interrupt callback functions
************************************************************************/
bool buttonPressedFlag = false;
bool chewPressedFlag = false;
bool rumPressedFlag = false;

static void buttonPressedInterrupt(void)
{
 buttonPressedFlag = true;
}
static void chewPressedInterrupt(void)
{
  chewPressedFlag = true;
}
static void rumPressedInterrupt(void)
{
  rumPressedFlag = true;
}

/***********************************************************************
check if button is pressed
returns true if button is pressed
***********************************************************************/
bool gpioIsButtonPressed(void)
{
  return buttonPressedFlag;
}
/***********************************************************************
if start button pressed returns 0x31('1')
if stop button pressed returns 0x32('2')
if nothing pressed returns -1
***********************************************************************/
int gpioGetCommand(void)
{
  if(chewPressedFlag)return 0x31;
  else if(rumPressedFlag)return 0x32;
  else return -1;
}

void gpioClearButtonPressed(void)
{
   buttonPressedFlag = false;
}
void gpioClearCommand(void)
{
  chewPressedFlag = false;
  rumPressedFlag = false;
}
/*******************************************************************************
 if startButton(PD6) still pressed returns 0x31
  if stopButton(PD4) still pressed returns 0x32
else returns -1
*******************************************************************************/
int gpioConfCommand(void)
{
  if((chewPressedFlag)&&(GPIO_PinInGet(CHEW_PORT,CHEW_PIN) == 0))
  {
    gpioClearCommand();
    return 0x31;
  }
  if((rumPressedFlag)&&(GPIO_PinInGet(RUM_PORT,RUM_PIN) == 0))
  {
    gpioClearCommand();
    return 0x32;
  }
  return -1;
}
/**************************************************************************//**
 * @brief GPIO Interrupt handler (PC14) PA0
 *        Sets the minutes
 *****************************************************************************/
#define GPIO_EVEN_MASK	0x55555555						///< Mask for even interrupts



void GPIO_EVEN_IRQHandler(void)
{  
  uint32_t mask;
  // Get interrupt bits
  mask = GPIO_IntGet() & GPIO_EVEN_MASK; 
  // Acknowledge interrupt 
  GPIO_IntClear(mask);
  for(int i = 0; i < hookSize; i++)
  {
    if(GPIO_Hooks[i].callback && (mask & GPIO_Hooks[i].mask))
      GPIO_Hooks[i].callback();
  }
  
}
/**************************************************************************//**
 * @brief GPIO Interrupt handler (PB10)
 *        Sets the minutes
 *****************************************************************************/

void GPIO_ODD_IRQHandler(void)
{
  uint32_t mask;
  // Get interrupt bits
  mask = GPIO_IntGet();
 // Acknowledge interrupt 
  GPIO_IntClear(mask);
  rumPressedInterrupt();
}
