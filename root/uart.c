#include <em_cmu.h>
#include <em_emu.h>
#include <stdbool.h>
#include <em_leuart.h>
#include <em_gpio.h>
#include "gpio.h"
#include "uart.h"

#define LEUART_TXPORT                 gpioPortD
#define LEUART_TXPIN                  4
#define LEUART_RXPORT                  gpioPortD        /* LEUART reception port    */
#define LEUART_RXPIN                   5                /* LEUART reception pin     */

/** Receive buffer size */
#define TD_UART_RXBUFSIZE               61

/*******************************************************************************
 *************************   TYPEDEFS   ****************************************
 ******************************************************************************/

/** @addtogroup UART_TYPEDEFS Typedefs
 * @{ */

/** UARt receive interrupt callback function pointer */
typedef void    (*TD_UART_CALLBACK)(char);

/** @} */

/*******************************************************************************
 **************************   PUBLIC VARIABLES   *******************************
 ******************************************************************************/

/** @addtogroup UART_PUBLIC_VARIABLES Public Variables
 * @{ */

/** UART receive callback function pointer */
volatile TD_UART_CALLBACK TD_UART_RxCallback = 0;

/** @} */

/*******************************************************************************
 *************************   PRIVATE VARIABLES   *******************************
 ******************************************************************************/

/** @addtogroup UART_PRIVATE_VARIABLES Private Variables
 * @{ */

/** share Flag for UART port with other GPIO function */
//static bool PortShared = false;

/** Enable flag for UART */
static LEUART_Enable_TypeDef PortEnable = leuartDisable;

/** UART Receive FIFO read index */
static int RxReadIndex = 0;

/** UART Receive FIFO write index */
static int RxWriteIndex = 0;

/** UART FIFO buffer */
static char RxBuffer[TD_UART_RXBUFSIZE];

/** @} */

/***************************************************************************//**
 * @brief
 *   Set up a callback function when receiving a character on UART.
 *
 * @param[in] cb
 *   Pointer to the UART receive character callback function.
 ******************************************************************************/
//static __INLINE void TD_UART_SetRxCallback(TD_UART_CALLBACK cb)
//{
//    TD_UART_RxCallback = cb;
//}
/***************************************************************************//**
 * @brief
 *   Initialize the UART peripheral.
 *
 * @param[in] speed
 *   The baudrate in bps.
 *
 * @param[in] rxEnable
 *   Enable flag.
 *
 * 
 ******************************************************************************/

void uartInit(uint32_t baudRate, bool rxEnable)
{
  
  //GPIO_PinModeSet(gpioPortD, 0, gpioModePushPull, 1);
  
  LEUART_Init_TypeDef init = LEUART_INIT_DEFAULT;                                         // defining the LEUART0 initialization data
  
  if (baudRate > 9600)
  {
    CMU_ClockSelectSet(cmuClock_LFB, cmuSelect_CORELEDIV2);
    CMU_ClockDivSet(cmuClock_LEUART0, cmuClkDiv_4);
    init.refFreq = 1750000;                                                             // 14MHz/2 prescaled by 4
  }
  else
  {
    CMU_ClockSelectSet(cmuClock_LFB, cmuSelect_LFXO);
    CMU_ClockDivSet(cmuClock_LEUART0, cmuClkDiv_1);
    init.refFreq = 0;
  }

  
  CMU_ClockEnable(cmuClock_LEUART0, true);                                                // enabling the LEUART0 clock
  
  init.enable     = leuartDisable;
  init.baudrate   = baudRate;
  
  LEUART_Reset(LEUART0);                                                                  // reseting and initializing LEUART0
  LEUART_Init(LEUART0, &init);
  
  /* Configure GPIO pins */
  CMU_ClockEnable(cmuClock_GPIO, true);
  /* To avoid false start, configure output as high */
  GPIO_PinModeSet(LEUART_TXPORT, LEUART_TXPIN, gpioModePushPull, 1);
  
  
  
  PortEnable = leuartEnableTx;                                                            // always enable tx
  
  if (rxEnable) {

        GPIO_PinModeSet(LEUART_RXPORT, LEUART_RXPIN, gpioModeInput, 0);                     // RX PORT

        // Setting the output GPIO register to 0 provides a 300 nA power saving
        // GPIO_PinOutClear(RX_PORT, RX_BIT);

        LEUART_IntClear(LEUART0, LEUART_IF_RXDATAV);                                        // clear previous RX interrupts
        NVIC_ClearPendingIRQ(LEUART0_IRQn);

        LEUART_IntEnable(LEUART0, LEUART_IF_RXDATAV);                                       // enable RX interrupts
        NVIC_EnableIRQ(LEUART0_IRQn);

        PortEnable |= leuartEnableRx;                                                       // enable rx
        LEUART0->ROUTE = LEUART_ROUTE_TXPEN | LEUART_ROUTE_RXPEN | LEUART_ROUTE_LOCATION_LOC0;
        LEUART0->CMD = LEUART_CMD_TXDIS | LEUART_CMD_RXDIS | LEUART_CMD_CLEARTX | LEUART_CMD_CLEARRX;
        LEUART0->CMD = LEUART_CMD_TXEN | LEUART_CMD_RXEN;
   }
  else{
        LEUART0->ROUTE = LEUART_ROUTE_TXPEN | LEUART_ROUTE_LOCATION_LOC0;
        LEUART0->CMD = LEUART_CMD_TXDIS | LEUART_CMD_CLEARTX;
        LEUART0->CMD = LEUART_CMD_TXEN;
  }
  LEUART_Enable(LEUART0, PortEnable);

}
/*******************************************************************************
send array to UART 
data - pointer to array
len - number of bytes to send
*******************************************************************************/
void uartSendArr(uint8_t *data, uint8_t len)
{
  for(int i = 0; i< len;i++)LEUART_Tx(LEUART0, data[i]);
}

/*******************************************************************************
sending byte to UART
*******************************************************************************/
void uartSend(uint8_t byte)
{
  LEUART_Tx(LEUART0, byte);
}
/******************************************************************************
sending message to Graph5
*******************************************************************************/

uint8_t uartData[13];  //message format 0x33 X1 X2 X3 X4 X5 X6

void uartGraph5Send( int16_t data,uint8_t point1,uint8_t point2)
{  
  uartData[0] = 0x33;
  uartData[1] = data >> 8;
  uartData[2] = data;         //X1

  uartData[3] = 0;
  uartData[4] = point1;        //X2
  uartData[5] = 0;
  uartData[6] = point2;        //X3
  uartSendArr(uartData, 13);
}

/***************************************************************************//**
 * @brief
 *   Receive a character from the UART.
 *
 * @return
 *   The received character if one is available, -1 otherwise.
 ******************************************************************************/
int uartGetChar(void)
{
    int c;

    if (RxReadIndex == RxWriteIndex) {
        return -1;
    }
    c = RxBuffer[RxReadIndex++];
    if (RxReadIndex == TD_UART_RXBUFSIZE) {
        RxReadIndex = 0;                                                                    // wrap Rx read Index
    }
    if (RxReadIndex == RxWriteIndex) {
        RxReadIndex = RxWriteIndex = 0;
    }
    return c;
}

/***************************************************************************//**
 * @brief
 *   UART interrupt handler.
 ******************************************************************************/
void LEUART0_IRQHandler(void)
{
    char data = LEUART0->RXDATA;                                                            // get the received byte

    if (TD_UART_RxCallback != 0) {                                                          // rx callback supplied
        (*TD_UART_RxCallback)(data);                                                        // call it
    }

    if (RxWriteIndex < TD_UART_RXBUFSIZE) {                                                         // enough space in receive buffer
        RxBuffer[RxWriteIndex++] = data;                                                    // save the received byte
    } else {
        if (RxWriteIndex == RxReadIndex) {                                                  // buffer overflow
            return;
        }
        RxWriteIndex = 0;
    }
}