
#include <em_cmu.h>
#include <em_emu.h>
#include <stdbool.h>
#include <em_leuart.h>
#include <em_gpio.h>
#include <intrinsics.h>
#include "buffer.h"

/*******************************************************************************
 *******************************   DEFINES   ***********************************
 ******************************************************************************/
#define MAX_STORE 5600

/*******************************************************************************
 **************************   GLOBAL FUNCTIONS   *******************************
 ******************************************************************************/

/***************************************************************************//**
 * @brief
 *   Add a new data to the buffer, 
 *   set storePointer
 * @param[in] data
 *  
 *
 *
 * @param[in] length of data to add
 *
 *
 *
 * @param[in] valid
 *
 *
 *@return
 *   true if there is an empty room in the buffer
 *   
 *   
 ******************************************************************************/
 uint16_t bufferData[MAX_STORE];

 int storePointer = 0, sendPointer = 0;
  bool bufferAdd( uint8_t* data, uint8_t length, uint8_t valid)
  {
    if(valid)
    {
      for(int i = 0; i < length/2;i++)  //save received data in buffer
      {       
        if(storePointer > MAX_STORE - 1)return false;
        bufferData[storePointer] = (data[i*2]<<8) + data[i*2 + 1];
        storePointer++;
      }
    }
    return true;
  }

/***************************************************************************//**
 * @brief
 *   Check if buffer contains data to send to Graph
 *
 *
 * @return
 *   true if has data 
 *   false if no data
 ******************************************************************************/
  bool bufferHasData(void)
  {
    if(sendPointer < storePointer)return true;
    return false;
  }

/***************************************************************************//**
 * @brief
 *   returns data to send to graph
 *   sets sendPointer
 *
 *
 * @return
 *   data to send to graph
 *   
 ******************************************************************************/
 int16_t bufferOutData(void)
 {
   if(sendPointer < storePointer)
   {
     sendPointer++;
     return bufferData[sendPointer-1];
   }
   return 0;
 }

/***************************************************************************//**
 * @brief
 *   Reset buffer stop sending data to Graph, ready to receive a new data from LoRa
 *   set storePointer
 * @param[in] None
 *  
 ******************************************************************************/
void bufferStop(void)
{
   storePointer = 0;
   sendPointer = 0;
}