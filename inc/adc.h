

void adcInit(void);

uint8_t adcPulseRead(void);

int32_t adcTempRead(void);