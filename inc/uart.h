#ifndef UART_H
#define UART_H
/***************************************************************************//**
 * @brief
 *   Initialize the UART peripheral.
 *
 * @param[in] speed
 *   The baudrate in bps.
 *
 * @param[in] rxEnable
 *   Enable flag.
 *
 * 
 ******************************************************************************/
void uartInit(uint32_t baudRate, bool rxEnable);
/******************************************************************************
sending message to Graph5
*******************************************************************************/

void uartGraph5Send( int16_t data,uint8_t point1,uint8_t point2);

void uartSend(uint8_t byte);

/***************************************************************************//**
 * @brief
 *   Receive a character from the UART.
 *
 * @return
 *   The received character if one is available, -1 otherwise.
 ******************************************************************************/
int uartGetChar(void);
/*******************************************************************************
send array to UART 
data - pointer to array
len - number of bytes to send
*******************************************************************************/
void uartSendArr(uint8_t *data, uint8_t len);


#endif // UART_H